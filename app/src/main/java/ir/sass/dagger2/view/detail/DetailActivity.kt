package ir.sass.dagger2.view.detail

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import ir.sass.dagger2.R

class DetailActivity : DaggerAppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        supportFragmentManager.beginTransaction().replace(R.id.frame,DetailFragment()).commit()
    }
}