package ir.sass.dagger2.view

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.RequestManager
import dagger.android.support.DaggerAppCompatActivity
import ir.sass.dagger2.R
import ir.sass.dagger2.view.detail.DetailActivity
import ir.sass.dagger2.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_auth.*
import javax.inject.Inject

class AuthActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var logo : Drawable
    @Inject
    lateinit var requestManager : RequestManager
    lateinit var viewModel : AuthViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var done = Observer<String>{
        Toast.makeText(this,it,Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        viewModel = ViewModelProvider(viewModelStore,viewModelFactory).get(AuthViewModel::class.java)
        viewModel.done.observe(this,done)
        viewModel.getUser(1)
//        startActivity(Intent(this,DetailActivity::class.java))
        setLogo()
    }

    fun setLogo()
    {
        requestManager.load(logo).into(img_logo)
    }
}
