package ir.sass.dagger2

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import ir.sass.dagger2.di.DaggerAppComponent

class App : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}