package ir.sass.dagger2.viewmodel

import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.sass.dagger2.network.auth.AuthApi
import ir.sass.dagger2.network.auth.model.UserModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class AuthViewModel @Inject constructor(): ViewModel() {
    @Inject
    lateinit var api : AuthApi

    val done  by lazy { MutableLiveData<String>() }

    fun getUser(id : Int)
    {

        api.getUser(id).enqueue(object : Callback<UserModel>{
            override fun onFailure(call: Call<UserModel>, t: Throwable) {
                done.value = "failed"
            }

            override fun onResponse(call: Call<UserModel>, response: Response<UserModel>) {
                if(response.isSuccessful)
                {
                    if(response.body()!!.title != null)
                    {
                        done.value = "ok"
                    }
                    else
                        done.value = "failed"
                }
                else
                    done.value = "failed"
            }
        })
    }

}