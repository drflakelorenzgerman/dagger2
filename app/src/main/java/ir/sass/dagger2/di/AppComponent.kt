package ir.sass.dagger2.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ir.sass.dagger2.App
import ir.sass.dagger2.di.viewmodel.ViewModelFactoryModule
import javax.inject.Singleton

// App is a client of AppComponent
@Singleton
@Component(modules =
[AndroidSupportInjectionModule::class, // special class
ActivityBuilderModule::class, ViewModelFactoryModule::class,
AppModule::class])
interface AppComponent : AndroidInjector<App> { // we are going to inject App
    @Component.Builder // override Builder
    interface Builder{
        @BindsInstance // bind an object to the component at time of its construction
        fun application(app : Application) : Builder

        fun build() : AppComponent
    }
//    fun inject(folan : Folan)
}