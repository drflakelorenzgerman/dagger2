package ir.sass.dagger2.di.detail

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.sass.dagger2.view.detail.DetailFragment

@Module
abstract class DetailFragmentBuilderModule {
    @ContributesAndroidInjector()
    abstract fun contributeDetailFragment() : DetailFragment

    // and other fragment DetailActivity has
}