package ir.sass.dagger2.di

import android.app.Application
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import dagger.Module
import dagger.Provides
import ir.sass.dagger2.R
import ir.sass.dagger2.network.auth.AuthApi
import ir.sass.dagger2.other.TAGS
import org.intellij.lang.annotations.PrintFormat
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    fun requestOptions(): RequestOptions {
        return RequestOptions
            .placeholderOf(R.drawable.loading)
            .error(R.drawable.error)
    }

    @Provides
    fun provideGlideInstance(application: Application, requestOptions: RequestOptions)
            : RequestManager {
        return Glide.with(application).setDefaultRequestOptions(requestOptions)
    }

    @Provides
    fun provideAppDrawble(application: Application): Drawable {
        return ContextCompat.getDrawable(application, R.drawable.logo)!!
    }

    @Singleton
    @Provides
    fun provideRetorfit(): Retrofit {
        return Retrofit.Builder().baseUrl(TAGS.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }

}