package ir.sass.dagger2.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.sass.dagger2.view.AuthActivity
import ir.sass.dagger2.di.auth.AuthModule
import ir.sass.dagger2.di.detail.DetailFragmentBuilderModule
import ir.sass.dagger2.di.scope.AuthScope
import ir.sass.dagger2.di.scope.DetailScope
import ir.sass.dagger2.di.viewmodel.AuthViewModelModule
import ir.sass.dagger2.view.detail.DetailActivity

@Module
abstract class ActivityBuilderModule {
    @AuthScope
    @ContributesAndroidInjector(modules = [AuthViewModelModule::class,AuthModule::class])
    abstract fun contributeAuthActivity() : AuthActivity
    // AuthActivity is a client , we can inject sth to it

    @DetailScope
    @ContributesAndroidInjector(modules = [DetailFragmentBuilderModule::class])
    abstract fun contributeDetailActivity() : DetailActivity
    // DetailActivity is a client , we can inject sth to it
}

