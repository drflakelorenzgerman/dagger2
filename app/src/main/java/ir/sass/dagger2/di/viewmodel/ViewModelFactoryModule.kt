package ir.sass.dagger2.di.viewmodel

import androidx.lifecycle.ViewModelProvider

import dagger.Binds
import dagger.Module
import ymz.yma.setareyek.base.ViewModelProviderFactory


@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelProviderFactory?): ViewModelProvider.Factory?
}