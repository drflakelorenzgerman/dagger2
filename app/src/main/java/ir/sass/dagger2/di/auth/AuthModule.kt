package ir.sass.dagger2.di.auth

import dagger.Module
import dagger.Provides
import ir.sass.dagger2.network.auth.AuthApi
import retrofit2.Retrofit

@Module
class AuthModule {
    @Provides fun provideApi(retrofit: Retrofit): AuthApi {
        return retrofit.create(AuthApi::class.java)
    }
}