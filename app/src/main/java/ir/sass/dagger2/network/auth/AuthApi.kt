package ir.sass.dagger2.network.auth

import ir.sass.dagger2.network.auth.model.UserModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface AuthApi {
    @GET("posts/{id}")
    fun getUser(@Path("id") userId : Int) : Call<UserModel>
}